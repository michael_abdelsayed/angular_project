﻿import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService } from '../_services';

@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }
    @Input() 
    pro:any;
    userId:string;
    firstName:string;
    lastName:string;
    username:string;
    password:string;
    DepartmentsList:any=[];
    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
            
        });
        this.loadDepartmentList();
    }
    loadDepartmentList(){
        this.userService.getAllDepartmentNames().subscribe((data:any)=>{
          this.DepartmentsList=data;
    
          this.userId=this.pro.userId;
        });
    }


    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

      
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
                this.pro={
                    userId:0,
                    firstName:"",
                    lastName:"",
                    username:"",
                    password:""
                  }
                var val = {userId:this.pro.userId,
                    firstName:this.firstName,
                    lastName:this.lastName,
                    username:this.username,
                    password:this.password
                    };
    
        this.userService.adduser(val).subscribe(res=>{
          
        });
                
    }
    adduser(){
        var val = {userId:this.userId,
                    firstName:this.firstName,
                    lastName:this.lastName,
                    username:this.username,
                    password:this.password
                    };
    
        this.userService.adduser(val).subscribe(res=>{
          alert(res.toString());
        });
      }
}
