﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { DepartmentComponent } from './department/department.component';
import { ShowDepComponent } from './department/show-dep/show-dep.component';
import { AddEditDepComponent } from './department/add-edit-dep/add-edit-dep.component';
import { EmployeeComponent } from './employee/employee.component';
import { ShowEmpComponent } from './employee/show-emp/show-emp.component';
import { AddEditEmpComponent } from './employee/add-edit-emp/add-edit-emp.component';
import {FormsModule} from "@angular/forms";
import { ProjectComponent } from './project/project.component';
import { AddEditProComponent } from './project/add-edit-pro/add-edit-pro.component';
import { ShowProComponent } from './project/show-pro/show-pro.component';
import { TaskComponent } from './task/task.component';
import { AddEditTaskComponent } from './task/add-edit-task/add-edit-task.component';
import { ShowTaskComponent } from './task/show-task/show-task.component';
import { ViewTaskComponent } from './task/view-task/view-task.component';;
import { NavbarComponent } from './navbar/navbar.component'

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        DepartmentComponent,
        ShowDepComponent,
        AddEditDepComponent,
        EmployeeComponent,
        ShowEmpComponent,
        AddEditEmpComponent,
        ProjectComponent ,
        AddEditProComponent ,
        ShowProComponent ,
        TaskComponent ,
        AddEditTaskComponent,
        ShowTaskComponent,
        ViewTaskComponent ,
        NavbarComponent     ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }