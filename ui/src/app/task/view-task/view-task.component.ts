import { Component, OnInit, Input} from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {

 @Input() task:any;
  taskId:string;
  task_project:string;
  task_emp:string;
  task_desc:string;
  task_work_hrs:string;
  EmployeeList:any=[];

  TaskList:any=[];

  ngOnInit(): void {
    this.loadEmployeeList();
  }
  loadEmployeeList(){
    
      this.taskId=this.task.taskId;
      this.task_project=this.task.task_project;
      this.task_emp=this.task.task_emp;
      this.task_desc=this.task.task_desc;
      this.task_work_hrs=this.task.task_work_hrs;
  }

}
