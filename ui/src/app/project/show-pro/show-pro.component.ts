import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-show-pro',
  templateUrl: './show-pro.component.html',
  styleUrls: ['./show-pro.component.css']
})
export class ShowProComponent implements OnInit {

  constructor(private service:UserService) { }

  ProjectList:any=[];
  ProjectListview:any=[];

  ModalTitle:string;
  ActivateAddEditProComp:boolean=false;
  pro:any;

  ngOnInit() {
  this.refreshProList();
  }

	addClick(){
	    this.pro={
	      projectId:0,
	      project_name:"",
	      project_url:""
	    }
	    this.ModalTitle="Add Project";
	    this.ActivateAddEditProComp=true;

  	}

  editClick(item){
    console.log(item);
    this.pro=item;
    this.ModalTitle="Edit Project";
    this.ActivateAddEditProComp=true;
  }

  deleteClick(item){
    if(confirm('Are you sure??')){
      this.service.deleteProject(item.projectId).subscribe(data=>{
        alert(data.toString());
        this.refreshProList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditProComp=false;
    this.refreshProList();
  }
  viewClick(item){
    
      this.service.viewProject(item.projectId).subscribe(data=>{
        this.ProjectListview=data;
      })
    }
  

  refreshProList(){
    this.service.getProList().subscribe(data=>{
      this.ProjectList=data;
    });
  }

}
