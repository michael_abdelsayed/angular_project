--------------Angular installation---------------
  
    1.Node
    2.Js
    3.Node Package manager

Typscript:
  
    $npm install -g typscript
    $tsc -v
  
Angular CLI:
 
    $npm install -g @angular/cli
    $ng -v
    $npm -v
    $node -v

Angula App create:

    $ng new project_name

Generate a component

    ng g c component_name

Remove Node Modules:

    $rm -rf  node_modules
