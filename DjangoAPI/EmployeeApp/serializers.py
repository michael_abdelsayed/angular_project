from rest_framework import serializers
from EmployeeApp.models import Projects, Employees, Tasks, Departments, Users

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('projectId',
                  'project_name',
                  'project_url',
                  )

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ('taskId',
                  'task_project',
                  'task_emp',
                  'task_desc',
                  'task_work_hrs',
                  )

class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departments
        fields = ('DepartmentId',
                  'DepartmentName')

class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employees
        fields = ('EmployeeId',
                  'EmployeeName',
                  'Department',
                  'DateOfJoining',
                  'PhotoFileName')

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('UserId',
                  'firstName',
                  'lastName',
                  'username',
                  'password')
                  