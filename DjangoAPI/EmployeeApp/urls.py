from django.conf.urls import url
from EmployeeApp import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns=[
    url(r'^department/$',views.departmentApi),
    url(r'^department/([0-9]+)$',views.departmentApi),

    url(r'^employee/$',views.employeeApi),
    url(r'^employee/([0-9]+)$',views.employeeApi),
    url(r'^login/$',views.emplogin),
    url(r'^user/$',views.userApi),
    url(r'^project/$',views.projectApi),
    url(r'^project/([0-9]+)$',views.projectApi),
    url(r'^task/$',views.taskApi),
    url(r'^task/([0-9]+)$',views.taskApi),

    url(r'^SaveFile$', views.SaveFile)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)