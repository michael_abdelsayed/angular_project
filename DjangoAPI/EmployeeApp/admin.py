from django.contrib import admin
from .models import Employees,Departments,Users,Projects,Tasks

admin.site.register(Employees)
admin.site.register(Departments)
admin.site.register(Users)
admin.site.register(Projects)
admin.site.register(Tasks)

# Register your models here.
