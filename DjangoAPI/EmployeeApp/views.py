from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from django.contrib.auth import authenticate,get_user_model,login,logout
from EmployeeApp.models import Departments,Employees,Users,Projects,Tasks
from EmployeeApp.serializers import DepartmentSerializer,EmployeeSerializer,UsersSerializer ,ProjectSerializer ,TaskSerializer
from django.conf import settings
from django.core.files.storage import default_storage
import os

# Create your views here.
@csrf_exempt
def departmentApi(request,id=0):
    if request.method=='GET':
        departments = Departments.objects.all()
        departments_serializer = DepartmentSerializer(departments, many=True)
        return JsonResponse(departments_serializer.data, safe=False)

    elif request.method=='POST':
        department_data=JSONParser().parse(request)
        department_serializer = DepartmentSerializer(data=department_data)
        if department_serializer.is_valid():
            department_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        department_data = JSONParser().parse(request)
        department=Departments.objects.get(DepartmentId=department_data['DepartmentId'])
        department_serializer=DepartmentSerializer(department,data=department_data)
        if department_serializer.is_valid():
            department_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        department=Departments.objects.get(DepartmentId=id)
        department.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)

@csrf_exempt
def employeeApi(request,id=0):
    if request.method=='GET':
        if id==0:
            employees = Employees.objects.all()
            employees_serializer = EmployeeSerializer(employees, many=True)
            return JsonResponse(employees_serializer.data, safe=False)
        else:
            employees = Employees.objects.get(EmployeeId=id)
            employees_serializer = EmployeeSerializer(employees)
            return JsonResponse(employees_serializer.data, safe=False)


    elif request.method=='POST':
        employee_data=JSONParser().parse(request)
        employee_serializer = EmployeeSerializer(data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        employee_data = JSONParser().parse(request)
        print(employee_data)
        employee=Employees.objects.get(EmployeeId=employee_data['EmployeeId'])
        employee_serializer=EmployeeSerializer(employee,data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        employee=Employees.objects.get(EmployeeId=id)
        employee.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)


@csrf_exempt
def SaveFile(request):
    file=request.FILES['uploadedFile']
    file_name = default_storage.save(file.name,file)

    return JsonResponse(file_name,safe=False)

@csrf_exempt
def emplogin(request,id=0):
    if request.method=='POST':
        user_values=[]
        employee_data=JSONParser().parse(request)
        if employee_data!={}:
            username=employee_data['username']
            password=employee_data['password']

            if employee_data!="":
                try:
                    user_info = Users.objects.get(username=username,password=password)
                    user_serializer = UsersSerializer(user_info, many=True)
                except:
                    user_info=""
               
                if user_info:
                    request.session['loggedin_user_username']=user_info.username
                    request.session['loggedin_user_name']=user_info.first_name+" "+user_info.last_name
                    if request.session['loggedin_user_username'] != "":
                        user_values.append(request.session['loggedin_user_username'])
                        user_values.append(request.session['loggedin_user_name'])
                        return JsonResponse(user_values,safe=False)
                    else:
                        return JsonResponse("Invalid login!.",safe=False)
                    # return HttpResponseRedirect("/home")
                else:
                    return JsonResponse("Invalid Credentials!.",safe=False)
                
        else:
            return JsonResponse("Please enter login credential",safe=False)

        return JsonResponse("Invalid Login!.",safe=False)

@csrf_exempt
def users(request,id=0):
    if request.method=='GET':
        Users = Users.objects.all()
        Users_serializer = Userserializer(Users, many=True)
        return JsonResponse(Users_serializer.data, safe=False)

    elif request.method=='POST':
        Users_data=JSONParser().parse(request)
        Users_serializer = UsersSerializer(data=Users_data)
        if Users_serializer.is_valid():
            Users_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        Users_data = JSONParser().parse(request)
        print(Users_data)
        Users=Users.objects.get(UsersId=Users_data['UsersId'])
        Users_serializer=Userserializer(Users,data=Users_data)
        if Users_serializer.is_valid():
            Users_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        Users=Users.objects.get(UsersId=id)
        Users.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)

@csrf_exempt
def projectApi(request,id=0):
    if request.method=='GET':
        if id==0:
            Project = Projects.objects.all()
            Project_serializer = ProjectSerializer(Project, many=True)
            return JsonResponse(Project_serializer.data, safe=False)
        else:
            Project = Projects.objects.get(projectId=id)
            Project_serializer = ProjectSerializer(Project)
            return JsonResponse(Project_serializer.data, safe=False)


    elif request.method=='POST':
        Project_data=JSONParser().parse(request)
        Project_serializer = ProjectSerializer(data=Project_data)
        if Project_serializer.is_valid():
            Project_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        Project_data = JSONParser().parse(request)
        print(Project_data)
        Project=Projects.objects.get(projectId=Project_data['projectId'])
        Project_serializer=ProjectSerializer(Project,data=Project_data)
        if Project_serializer.is_valid():
            Project_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        Project=Projects.objects.get(projectId=id)
        Project.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)

@csrf_exempt
def taskApi(request,id=0):
    if request.method=='GET':
        if id==0:
            Task = Tasks.objects.all()
            Task_serializer = TaskSerializer(Task, many=True)
            return JsonResponse(Task_serializer.data, safe=False)
        else:
            Task = Tasks.objects.get(taskId=id)
            Task_serializer = TaskSerializer(Task)
            return JsonResponse(Task_serializer.data, safe=False)


    elif request.method=='POST':
        Task_data=JSONParser().parse(request)
        Task_serializer = TaskSerializer(data=Task_data)
        if Task_serializer.is_valid():
            Task_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        Task_data = JSONParser().parse(request)
        print(Task_data)
        Task=Tasks.objects.get(taskId=Task_data['taskId'])
        Task_serializer=TaskSerializer(Task,data=Task_data)
        if Task_serializer.is_valid():
            Task_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        Task=Tasks.objects.get(taskId=id)
        Task.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)
@csrf_exempt
def userApi(request,id=0):
    if request.method=='GET':
        if id==0:
            user = Users.objects.all()
            user_serializer = UsersSerializer(user, many=True)
            return JsonResponse(user_serializer.data, safe=False)
        else:
            user = User.objects.get(userId=id)
            user_serializer = UsersSerializer(Task)
            return JsonResponse(user_serializer.data, safe=False)
    
    if request.method=='POST':
        user_data=JSONParser().parse(request)
        print(user_data)
        user_serializer = UsersSerializer(data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    



